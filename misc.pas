unit Misc;
{ Dostarcza pewnych typow wyliczeniowych i stalych
  wykorzystywanych w calym projekcie.
}
{$mode objfpc}{$H+}

interface

uses
  Classes, SysUtils;

type
  TDirectionEnum = (HORIZONTAL, VERTICAL, LRTB, LRBT);
  TPlayerEnum = (P1, P2, NONE);
const
{$IFDEF WINDOWS}
SLASH = '\';
{$ELSE}
SLASH = '/';
{$ENDIF}
SAVE_FOLDER = 'saves';
SAVE_EXTENSION = '.sav';
RES_FOLDER = 'resources';
SPRITE_FOLDER = RES_FOLDER + SLASH + 'sprites';
SEGMENT_FOLDER = 'segment';
VERTEX_FOLDER = 'vertice';
TEMPLATE_FOLDER = RES_FOLDER + SLASH + 'field_templates';
TEMPLATE_EXTENSION = '.ftmp';

implementation

end.

