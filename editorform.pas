unit EditorForm;

{$mode objfpc}{$H+}

interface

uses
  Classes, SysUtils, FileUtil, Forms, Controls, Graphics,
  Dialogs, ExtCtrls, StdCtrls, ComCtrls, Game, FieldClasses, Misc{, Player};

type

  { TEditForm }

  TEditForm = class(TForm)
    Player2Net: TPanel;
    Player2Net1: TPanel;
    PresetNameEdit: TEdit;
    PresetNameLabel: TLabel;
    SavePresetButton: TButton;
    FieldWidthSetterLabel: TLabel;
    FieldHeightSetterLabel: TLabel;
    ChangeBoundsCautionLabel: TLabel;
    SetBoundsButton: TButton;
    PlayButton: TButton;
    EditionPanel: TPanel;
    EditorModeMenu: TRadioGroup;
    EditorMenu: TGroupBox;
    StartingPlayerMenu: TRadioGroup;
    FieldWidthTrackBar: TTrackBar;
    FieldHeightTrackBar: TTrackBar;
    procedure FieldHeightTrackBarChange(Sender: TObject);
    procedure FieldWidthTrackBarChange(Sender: TObject);
    procedure FormClose(Sender: TObject; var CloseAction: TCloseAction);
    procedure FormCloseQuery(Sender: TObject; var CanClose: boolean);
    procedure FormCreate(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure EditorModeMenuSelectionChanged(Sender: TObject);
    procedure PlayButtonClick(Sender: TObject);
    procedure SavePresetButtonClick(Sender: TObject);
    procedure SetBoundsButtonClick(Sender: TObject);
    procedure StartingPlayerMenuSelectionChanged(Sender: TObject);
  private
    chosenQuickplay : Boolean;
  public
    { public declarations }
  end;
  TGameCreator = class
      type TEditorModeEnum = (BORDER_PLACEMENT, BALL_PLACEMENT, NONE);
      constructor Create;
      destructor Destroy;
      procedure OnChosenVertex(Sender: TObject);
      //function NewGame(AOwner : TWinControl; GameInfoPanel : TPanel; FieldWidth, FieldHeight : Integer) : TGame;
      private
             editionMode : TEditorModeEnum;
             mNewGame : TGame;
             editorField : TField;
             editorFieldTemplate : TFieldTemplate;
             previouslyChosenVertex : TVertex;
      public
             property newGame : TGame Read mNewGame Write mNewGame;
  end;

var
  EditForm: TEditForm;
  gameStateData : TGameCreator;

implementation
{
  Glowna idea edytora: tworzy Kreator Gry, ktory jako widoczna dla uzytkownika czesc
  udostepnia plansze edytora z wlasnym specyficznym zdarzeniem OnClick na wezlach,
  zas w tle tworzy gre podlug tego, co gracz wyklika w edytorze.
}
uses
    GameForm, MainMenu, DataParser;

const
     PLAY_BUTTON_DEFAULT_MSG = 'Quickplay VS CPU';
     PLAY_BUTTON_DURING_GAME_MSG = 'Continue game';
     CAUTION_LABEL_DEFAULT_MSG = 'Warning: changing width or height of the field will result in losing other changes made priorly.';
     CAUTION_LABEL_DURING_GAME_MSG = 'Bounds cannot be changed during a game.';
     DEFAULT_PRESET_NAME = 'field';
{$R *.lfm}

{ TEditForm }

procedure TEditForm.FormClose(Sender: TObject; var CloseAction: TCloseAction);
begin
  EditorModeMenuSelectionChanged(Sender);
  if FootballGameForm.EditDuringGame then begin
     { Jesli gracz zamknie okno edycji wywolanej w trakcie gry
       edytor wznowi gre nawet jesli przycisk kontynuacji gry nie zostal wcisniety -
       - gre zawsze mozna zamknac, a chroni to przed przypadkowa
       utrata pracy itp }
     gameStateData.newGame.GameField.Template := gameStateData.editorFieldTemplate;
       gameStateData.editorField.Template := nil;
     FootballGameForm.CurrentGame.GameField.Template := nil;
     Self.Hide;
     FootballGameForm.Show;
     FootballGameForm.CurrentGame.Start;
  end else if chosenQuickplay then begin
     gameStateData.newGame.GameField.Template.Destroy;
     gameStateData.newGame.GameField.Template := gameStateData.editorFieldTemplate;
     gameStateData.editorField.Template := nil;
     chosenQuickplay := false;
     FootballGameForm.CurrentGame := gameStateData.newGame;
     Self.Hide;
     FootballGameForm.Show;
     FootballGameForm.CurrentGame.Start;
  end else begin
     gameStateData.newGame.Destroy;
     Self.Hide;
     Football.Show;
  end;
  gameStateData.Destroy;
  gameStateData := nil;
end;

procedure TEditForm.FormCloseQuery(Sender: TObject; var CanClose: boolean);
const
     QUIT_EDITOR_DIALOG_NAME = 'Exit to main menu';
     QUIT_EDITOR_DIALOG_QUESTION = 'Warning: You will lose any unsaved changes. Do you still want to exit?';
begin
    if MessageDlg(QUIT_EDITOR_DIALOG_NAME, QUIT_EDITOR_DIALOG_QUESTION, mtConfirmation,
              [mbYes, mbNo],0) = mrYes then begin
        CanClose := true;
    end else
        CanClose := false;
end;

procedure TEditForm.FieldWidthTrackBarChange(Sender: TObject);
begin
     if Odd(FieldWidthTrackBar.Position) then
        FieldWidthTrackBar.Position:=FieldWidthTrackBar.Position + 1;
end;

procedure TEditForm.FieldHeightTrackBarChange(Sender: TObject);
begin
     if Odd(FieldHeightTrackBar.Position) then
        FieldHeightTrackBar.Position:=FieldHeightTrackBar.Position + 1;
end;

procedure TEditForm.FormCreate(Sender: TObject);
begin
    gameStateData := nil;
    chosenQuickplay:=false;
end;

procedure TEditForm.FormShow(Sender: TObject);
begin
    if gameStateData = nil then
       gameStateData := TGameCreator.Create;
    EditorModeMenu.ItemIndex:=0;
    StartingPlayerMenu.ItemIndex:=0;
    SetBoundsButton.Enabled:=(not FootballGameForm.EditDuringGame);
    PresetNameEdit.Text:=DEFAULT_PRESET_NAME+IntToStr(Football.FieldTemplateList.Count);
    if FootballGameForm.EditDuringGame then begin
       PlayButton.Caption:=PLAY_BUTTON_DURING_GAME_MSG;
       ChangeBoundsCautionLabel.Caption:=CAUTION_LABEL_DURING_GAME_MSG;
    end else begin
       PlayButton.Caption:=PLAY_BUTTON_DEFAULT_MSG;
       ChangeBoundsCautionLabel.Caption:=CAUTION_LABEL_DEFAULT_MSG;
    end;
end;

procedure TEditForm.EditorModeMenuSelectionChanged(Sender: TObject);
begin
    if (gameStateData.previouslyChosenVertex <> nil) and
       (gameStateData.previouslyChosenVertex.IsChecked) then begin
          gameStateData.previouslyChosenVertex.UnCheck;
          if gameStateData.newGame.GameField.GetVertexAtPos(gameStateData.previouslyChosenVertex.fposX, gameStateData.previouslyChosenVertex.fposY).IsChecked then
             gameStateData.newGame.GameField.GetVertexAtPos(gameStateData.previouslyChosenVertex.fposX, gameStateData.previouslyChosenVertex.fposY).UnCheck;
          gameStateData.previouslyChosenVertex := nil;
          gameStateData.editorField.SetBall(gameStateData.editorField.Ball);
          gameStateData.newGame.GameField.SetBall(gameStateData.newGame.GameField.Ball);
    end;
    case TRadioGroup(Sender).ItemIndex of
         0 : gameStateData.editionMode := BORDER_PLACEMENT;
         1 : gameStateData.editionMode := BALL_PLACEMENT;
    end;

end;

procedure TEditForm.PlayButtonClick(Sender: TObject);
begin
    chosenQuickplay := true;
    EditForm.Close;
end;

procedure TEditForm.SavePresetButtonClick(Sender: TObject);
const
     TEMPLATE_SAVE_FAIL = 'Error: can not save the template.';
begin
   try
   if DataManager.IsProperFileName(PresetNameEdit.Text) then begin
      if Football.FieldTemplateList.Items.IndexOf(PresetNameEdit.Text) >= 0 then begin
         if MessageDlg(OVERWRITE_MESSAGE_DIALOG_NAME, OVERWRITE_MESSAGE_DIALOG_QUESTION, mtConfirmation,
            [mbYes, mbNo],0) = mrYes then begin
            DataManager.SaveFieldTemplate(gameStateData.editorField, PresetNameEdit.Text);
            ShowMessage(FILE_SAVED_SUCCESFULLY);
         end;
      end else begin
            DataManager.SaveFieldTemplate(gameStateData.editorField, PresetNameEdit.Text);
            Football.FieldTemplateList.AddItem(PresetNameEdit.Text, nil);
            ShowMessage(FILE_SAVED_SUCCESFULLY);
      end;
   end;
   except
         ShowMessage(TEMPLATE_SAVE_FAIL);
   end;
end;

procedure TEditForm.SetBoundsButtonClick(Sender: TObject);
begin
    gameStateData.mNewGame.Destroy;
    gameStateData.Destroy;
    gameStateData := TGameCreator.Create;
end;

procedure TEditForm.StartingPlayerMenuSelectionChanged(Sender: TObject);
begin
    if gameStateData <> nil then
       gameStateData.newGame.CurrentPlayerId:=TRadioGroup(Sender).ItemIndex;
end;


destructor TGameCreator.Destroy;
begin
    editorField.Destroy;
    mNewGame := nil;
end;

constructor TGameCreator.Create;
begin
    if EditForm.EditorModeMenu.ItemIndex = 0 then
       editionMode:=BORDER_PLACEMENT
    else
        editionMode := BALL_PLACEMENT;
    if FootballGameForm.EditDuringGame then begin
        editorField := TField.CreateFromTemplate(EditForm.EditionPanel, FootballGameForm.CurrentGame.GameField.Template, @OnChosenVertex);
        editorField.SetBall(editorField.GetVertexAtPos(FootballGameForm.CurrentGame.GameField.Ball.fposX, FootballGameForm.CurrentGame.GameField.Ball.fposY));
        newGame := TGame.CreateFromFieldTemplate(FootballGameForm.GamePanel, FootballGameForm.GameInfoPanel, FootballGameForm.CurrentGame.GameField.Template, FootballGameForm.CurrentGame.Player[0].IsHuman, FootballGameForm.CurrentGame.Player[1].IsHuman);
        newGame.GameField.SetBall(newGame.GameField.GetVertexAtPos(FootballGameForm.CurrentGame.GameField.Ball.fposX, FootballGameForm.CurrentGame.GameField.Ball.fposY));
        EditForm.FieldWidthTrackBar.Position:=FootballGameForm.CurrentGame.GameField.FieldWidth;
        EditForm.FieldHeightTrackBar.Position:=FootballGameForm.CurrentGame.GameField.FieldHeight;
        EditForm.StartingPlayerMenu.ItemIndex:=FootballGameForm.CurrentGame.CurrentPlayerId;
    end else begin
        editorField := TField.Create(EditForm.EditionPanel, EditForm.FieldWidthTrackBar.Position, EditForm.FieldHeightTrackBar.Position, @OnChosenVertex);
        newGame := TGame.Create(FootballGameForm.GamePanel, FootballGameForm.GameInfoPanel, EditForm.FieldWidthTrackBar.Position, EditForm.FieldHeightTrackBar.Position, true, false);
    end;
    editorFieldTemplate := editorField.Template;
    editorFieldTemplate.BallPosX:=editorField.Ball.fposX;
    editorFieldTemplate.BallPosY:=editorField.Ball.fposY;
    editorFieldTemplate.FieldHeight:=editorField.FieldHeight;
    editorFieldTemplate.FieldWidth:=editorField.FieldWidth;
    previouslyChosenVertex := nil;
end;

procedure TGameCreator.OnChosenVertex(Sender : TObject);

var s : TSegment;
    chosenVertex, oldBall, chosenVertexAtGame, previouslyChosenVertexAtGame : TVertex;
begin
     chosenVertex := TVertex(Sender);
     case editionMode of

          BALL_PLACEMENT : begin
            oldBall := editorField.Ball;
            if oldBall <> chosenVertex then begin
            editorField.SetBall(chosenVertex);
            newGame.GameField.SetBall(newGame.GameField.GetVertexAtPos(chosenVertex.fposX, chosenVertex.fposY));
            if oldBall.IsChecked then
               oldBall.UnCheck;
            if newGame.GameField.GetVertexAtPos(oldBall.fposX, oldBall.fposY).IsChecked then
               newGame.GameField.GetVertexAtPos(oldBall.fposX, oldBall.fposY).UnCheck;
            editorFieldTemplate.BallPosX:=editorField.Ball.fposX;
            editorFieldTemplate.BallPosY:=editorField.Ball.fposY;
            end;
          end;

          BORDER_PLACEMENT : begin
     if (previouslyChosenVertex = nil) then begin
        previouslyChosenVertex := chosenVertex;
        if not chosenVertex.IsChecked then
           chosenVertex.Check;
     end else if (chosenVertex = previouslyChosenVertex) then begin
        if previouslyChosenVertex.IsChecked then
           previouslyChosenVertex.UnCheck;
        previouslyChosenVertex := nil;
        editorField.SetBall(editorField.Ball);
     end else if (chosenVertex.IsNeighbourOf(previouslyChosenVertex)) then begin
        if chosenVertex.CanBeConnectedTo(previouslyChosenVertex) then begin
             chosenVertexAtGame := newGame.GameField.GetVertexAtPos(chosenVertex.fposX, chosenVertex.fposY);
             previouslyChosenVertexAtGame := newGame.GameField.GetVertexAtPos(previouslyChosenVertex.fposX, previouslyChosenVertex.fposY);
             previouslyChosenVertex.SetPlacement(TVertex.TVertexPlacement.BORDER);
             chosenVertex.SetPlacement(TVertex.TVertexPlacement.BORDER);
             previouslyChosenVertex.RefreshImage;
             chosenVertex.RefreshImage;
             s := chosenVertex.MakeConnectionTo(previouslyChosenVertex, TPlayerEnum.NONE);
             if not chosenVertex.IsChecked then
                chosenVertex.Check;
             if not chosenVertexAtGame.IsChecked then
                chosenVertexAtGame.Check;
             editorField.AddSegmentToBorderList(s);
             editorFieldTemplate.AddCustomConnection(previouslyChosenVertex.fposX, previouslyChosenVertex.fposY, chosenVertex.fposX, chosenVertex.fposY);
             s := newGame.GameField.GetVertexAtPos(chosenVertex.fposX, chosenVertex.fposY).MakeConnectionTo(newGame.GameField.GetVertexAtPos(previouslyChosenVertex.fposX, previouslyChosenVertex.fposY), TPlayerEnum.NONE);                                                                                                                                                                                                                          chosenVertexAtGame.SetPlacement(TVertex.TVertexPlacement.BORDER);
             previouslyChosenVertexAtGame.SetPlacement(TVertex.TVertexPlacement.BORDER);
             chosenVertexAtGame.RefreshImage;
             previouslyChosenVertexAtGame.RefreshImage;
             newGame.GameField.AddSegmentToBorderList(s);
             if previouslyChosenVertexAtGame.IsChecked then
                previouslyChosenVertexAtGame.UnCheck;
             if previouslyChosenVertex.IsChecked then
                previouslyChosenVertex.UnCheck;
             editorField.SetBall(editorField.Ball);
             newGame.GameField.SetBall(newGame.GameField.GetVertexAtPos(editorField.Ball.fposX, editorField.Ball.fposY));
             previouslyChosenVertex := chosenVertex;
        end;
     end;
     end;
     end;
end;

end.

