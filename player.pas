unit Player;

{$mode objfpc}{$H+}

interface

uses
  Classes, SysUtils, Forms, FieldClasses, Misc;

type
    TPlayer = class abstract
         constructor Create(index : Integer);
         procedure MakeMove(Field : TField); virtual; abstract;
         function IsHuman : Boolean; virtual; abstract;
         private
              mPlayerName : String;
              mPlayerIndex : TPlayerEnum;
              mPlayerNumber : Integer;
              mCanMakeMove : Boolean;
         public
              property playerName : String Read mPlayerName;
              property playerIndex : TPlayerEnum Read mPlayerIndex;
              property playerNumber : Integer Read mPlayerNumber;
              property canMakeMove : Boolean Read mCanMakeMove Write mCanMakeMove;
    end;
    TPlayerHuman = class(TPlayer)
         procedure MakeMove(Field : TField); override;
         function IsHuman : Boolean; override;
    end;
    TPlayerCPU = class(TPlayer)
         constructor Create(index : Integer);
         function ChoosePathWisely(Field : TField) : TVertex;
         procedure MakeMove(Field : TField); override;
         function IsHuman : Boolean; override;

    end;

CONST P1_PLAYERNAME = 'blue';
      P2_PLAYERNAME = 'red';

implementation

function TPlayerHuman.IsHuman : Boolean;
begin
     IsHuman := true;
end;

function TPlayerCPU.IsHuman : Boolean;
begin
     IsHuman := false;
end;

constructor TPlayer.Create(index : Integer);
begin
     mPlayerNumber:=index;
     mCanMakeMove := true;
     if index = 0 then begin
        mPlayerIndex := P1;
        mPlayerName := P1_PLAYERNAME;
     end
     else begin
        mPlayerIndex := P2;
        mPlayerName := P2_PLAYERNAME;
     end;
end;

procedure TPlayerHuman.MakeMove(Field : TField);
begin
end;

constructor TPlayerCPU.Create(index : Integer);
begin
     Inherited Create(index);
end;

function TPlayerCPU.ChoosePathWisely(Field : TField) : TVertex;

  function GetVertexGrade(V : TVertex) : Integer;
  var grade : Integer;

      function DistanceFromCenterHorizontallyBonus : Integer;
      var multiplier : Integer;
      begin
          if (playerNumber = 0) xor (Field.Ball.fposY >= (Field.FieldHeight+2)div 2) then
             multiplier := (-1) // jestem po stronie przeciwnika wiec jesli sie oddalam od bramki to zle
          else
             multiplier := 1;   // jestem po swojej stronie boiska wiec jesli sie oddalam od bramki to dobrze

          if V.fposX >= Field.FieldWidth div 2 + 1 then
             DistanceFromCenterHorizontallyBonus := multiplier * ( V.fposX - (Field.FieldWidth div 2 + 1) )
          else if V.fposX < Field.FieldWidth div 2 + 1 then
             DistanceFromCenterHorizontallyBonus := multiplier * ( (Field.FieldWidth div 2 + 1) - V.fposX );
      end;

  begin
       grade := 0;
       if canMakeMove then begin
       grade := grade + abs((Field.FieldHeight+2)*(1-playerNumber) - V.fposY)*4; // im dalej od swojej bramki tym lepiej
       grade := grade + DistanceFromCenterHorizontallyBonus;
       if V.GetConnectionCount > 0 then
          grade := grade + 4; // fajnie jak mozna sie dalej odbic
       if (Field.IsVertexInsideNet(V)) then begin
          if (abs((Field.FieldHeight+2)*(1-playerNumber) - V.fposY) >= Field.FieldHeight+2 div 2) then
             grade := MaxInt    // jesli wpada do bramki po przeciwnej stronie boiska niz moja bramka
          else
             grade := -MaxInt+1;  // w przeciwnym wypadku
       end else if (V.GetConnectionCount = 7) then
                   grade := -MaxInt+2;
       GetVertexGrade := grade;
       end;
  end;

  function chooseVertex : TVertex;
  // zachlanny algorytm : wybieramy najlepszy fragment ruchu z obecnego punktu widzenia.
  var i, count, interestingCount, bestIndex, currentGrade, highestGrade : Integer;
      directions : array[0..7] of TVertex;
  begin
       count := -1;
       interestingCount := -1;
       for i := 0 to 7 do begin
           if not (Field.Ball.IsConnectedInDirection(i)) then begin
              directions[count+1] := Field.GetVertexNeighbourInDirection(Field.Ball, i);
              inc(count);
              if directions[count].GetConnectionCount < 7 then
                 inc(interestingCount);
           end;
       end;
       if (interestingCount = -1) then // wszystkie pozycje sa przegrywajace
          chooseVertex := directions[Random(count)]
       else begin
          bestIndex := -1;
          highestGrade := -MaxInt;
          for i := 0 to count do begin
              currentGrade := GetVertexGrade(directions[i]);
              if currentGrade > highestGrade then begin
                 bestIndex := i;
                 highestGrade := currentGrade;
              end;
              chooseVertex := directions[bestIndex];
          end;
       end;
  end;

begin
     ChoosePathWisely := chooseVertex;
end;

procedure TPlayerCPU.MakeMove(Field : TField);
var chosenVertex : TVertex;

begin
    if canMakeMove then begin
    Application.ProcessMessages;
    chosenVertex := ChoosePathWisely(Field);
    Sleep(500);
    chosenVertex.OnClick(chosenVertex);
    end;
end;

end.

