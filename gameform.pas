unit GameForm;

{$mode objfpc}{$H+}

interface

uses
  Classes, SysUtils, FileUtil, Forms, Controls, Graphics, Dialogs, StdCtrls,
  ExtCtrls, Game, DataParser;

type

  { TFootballGameForm }

  TFootballGameForm = class(TForm)
    EditorButton: TButton;
    ExitButton: TButton;
    HintButton: TButton;
    Player1Net: TPanel;
    Player2Net: TPanel;
    RepeatMoveLabel: TLabel;
    SaveButton: TButton;
    UndoSegmentLabel: TLabel;
    UndoSegmentButton: TButton;
    RepeatSegmentButton: TButton;
    UndoMoveButton: TButton;
    RepeatMoveButton: TButton;
    GamePanel: TPanel;
    GameInfoPanel: TPanel;
    CurrentPlayerTitle: TLabel;
    CurrentPlayerLabel: TLabel;
    RepeatSegmentLabel: TLabel;
    UndoMoveLabel: TLabel;
    procedure EditorButtonClick(Sender: TObject);
    procedure ExitButtonClick(Sender: TObject);
    procedure FormClose(Sender: TObject; var CloseAction: TCloseAction);
    procedure FormCloseQuery(Sender: TObject; var CanClose: boolean);
    procedure FormCreate(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure HintButtonClick(Sender: TObject);
    procedure RepeatMoveButtonClick(Sender: TObject);
    procedure RepeatSegmentButtonClick(Sender: TObject);
    procedure SaveButtonClick(Sender: TObject);
    procedure UndoMoveButtonClick(Sender: TObject);
    procedure UndoSegmentButtonClick(Sender: TObject);
  private
  public
    CurrentGame : TGame;
    EditDuringGame : Boolean;
  end;

var
  FootballGameForm: TFootballGameForm;

implementation
uses
    EditorForm, MainMenu;
{var
  CurrentGame : TGame;}


{$R *.lfm}

{ GameForm }
procedure TFootballGameForm.FormCreate(Sender: TObject);

begin
    //LoadGraphics(FootballGameForm);
    CurrentGame := nil;
    EditDuringGame:=false;
end;

procedure TFootballGameForm.FormShow(Sender: TObject);
begin
    if (CurrentGame <> nil) and (EditDuringGame) then begin
       EditDuringGame := false;
       CurrentGame.Destroy;
       CurrentGame := nil;
    end;
    if (gameStateData <> nil) and (CurrentGame = nil) then begin
       CurrentGame := gameStateData.newGame;
    end;
end;

procedure TFootballGameForm.FormClose(Sender: TObject; var CloseAction: TCloseAction);
begin
    if CurrentGame <> nil then
       CurrentGame.Destroy;
    CurrentGame := nil;
    Self.Hide;
    Football.Show;
end;

procedure TFootballGameForm.FormCloseQuery(Sender: TObject;
  var CanClose: boolean);
const
     QUIT_GAME_DIALOG_NAME = 'Exit to main menu';
     QUIT_GAME_DIALOG_QUESTION = 'Warning: You will lose any unsaved progress. Do you still want to exit?';
begin
    if (not CurrentGame.GameEnded) then begin
        if (MessageDlg(QUIT_GAME_DIALOG_NAME, QUIT_GAME_DIALOG_QUESTION, mtConfirmation,
           [mbYes, mbNo],0) = mrYes) then begin
           CanClose := true;
        end else
           CanClose := false;
    end else
        CanClose := true;
end;

procedure TFootballGameForm.ExitButtonClick(Sender: TObject);
begin
    Self.Close;
end;

procedure TFootballGameForm.EditorButtonClick(Sender: TObject);
begin
    EditDuringGame:=true;
    Self.Hide;
    EditForm.Show;
end;

procedure TFootballGameForm.HintButtonClick(Sender: TObject);
begin
    HintButton.Enabled:=false;
    CurrentGame.GiveHint;
end;

procedure TFootballGameForm.RepeatMoveButtonClick(Sender: TObject);
begin
    CurrentGame.RepeatMove;
end;

procedure TFootballGameForm.RepeatSegmentButtonClick(Sender: TObject);
begin
    CurrentGame.RepeatSegment;
end;

procedure TFootballGameForm.SaveButtonClick(Sender: TObject);
const
     SAVE_GAME_DIALOG_NAME = 'Save game';
     SAVE_GAME_DIALOG_QUESTION = 'Name:';
     SAVE_GAME_MESSAGE_DIALOG_QUESTION = 'Game save with such name already found. Do you wish to overwrite already existing save?';
     DEFAULT_SAVE_NAME = 'save';
     GAME_SAVED_SUCCESFULLY = 'Game saved succesfully.';
     SAVE_GAME_FAILED = 'Error: cannot save the game.';
var
   fileName : AnsiString;
begin
    try
    fileName := DEFAULT_SAVE_NAME+IntToStr(Football.GameSavesList.Count);
    if InputQuery(SAVE_GAME_DIALOG_NAME, SAVE_GAME_DIALOG_QUESTION, fileName) and DataManager.IsProperFileName(fileName) then
    if Football.GameSavesList.Items.IndexOf(fileName) >= 0 then begin
      if MessageDlg(SAVE_GAME_DIALOG_NAME, SAVE_GAME_MESSAGE_DIALOG_QUESTION, mtConfirmation,
            [mbYes, mbNo],0) = mrYes then begin
            DataManager.SaveGame(CurrentGame, fileName);
            ShowMessage(GAME_SAVED_SUCCESFULLY);
      end;
    end else begin
            DataManager.SaveGame(CurrentGame, fileName);
            Football.GameSavesList.AddItem(fileName, nil);
            ShowMessage(GAME_SAVED_SUCCESFULLY);
    end;
    except
        ShowMessage(SAVE_GAME_FAILED);
    end;
end;

procedure TFootballGameForm.UndoMoveButtonClick(Sender: TObject);
begin
    CurrentGame.UndoMove;
end;

procedure TFootballGameForm.UndoSegmentButtonClick(Sender: TObject);
begin
    CurrentGame.UndoSegment;
end;

end.

