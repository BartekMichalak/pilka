unit Game;

{$mode objfpc}{$H+}

interface

uses
  Classes, SysUtils, Controls, Dialogs {debug!}, FieldClasses, Forms, Graphics, {StdCtrls,}
  ExtCtrls, Misc, Player;

type

    PMoveList = ^TMoveListElement;
    TMoveListElement = record
      Segment : TSegment;
      Direction : Integer;
      IsEndMove : Boolean;
      Next : PMoveList;
    end;
    TStack = class
      Stack : PMoveList;
      constructor Create;
      destructor Destroy;
      function Pop : PMoveList;
      function Top : PMoveList;
      procedure Push(move : PMoveList);
      function IsEmpty : Boolean;
      procedure RemoveAll;
    end;
    TGameHistory = class
      constructor Create;
      destructor Destroy;
      function GetNextPartialMove : PMoveList;
      function GetPreviousPartialMove : PMoveList;
      procedure AddSegmentToUndo(moveData : PMoveList);
      procedure AddSegmentToRepeat(moveData : PMoveList);
      procedure ClearUndoHistory;
      procedure ClearRepeatHistory;
      procedure ClearHistory;
      function GetMoveToSave : PMoveList;
      private
            hasPrevSeg, hasPrevMove, hasNextSeg, hasNextMove : Boolean;
            DoneMoves, UnDoneMoves : TStack;
      public
            property hasDoneSegments : Boolean Read hasPrevSeg;
            property hasUnDoneSegments : Boolean Read hasNextSeg;
            property hasUnDoneMoves : Boolean Read hasNextMove;
            property hasDoneMoves : Boolean Read hasPrevMove;
    end;
    TGame = class
      GameField : TField;
      Player : array[0..1] of TPlayer;
      CurrentPlayerId : Integer;
      GameInfo : TPanel;
      History : TGameHistory;
      HintPlayer : TPlayer;
      HistoryLoading, GameEnded : Boolean;
      constructor Create(AOwner : TWinControl; GameInfoPanel : TPanel; X, Y : Integer; IsPlayerOneHuman, IsPlayerTwoHuman : Boolean);
      constructor CreateFromFieldTemplate(AOwner : TWinControl; GameInfoPanel : TPanel; FieldTemplate : TFieldTemplate; IsPlayerOneHuman, IsPlayerTwoHuman : Boolean);
      destructor Destroy;
      procedure OnChosenVertex(Sender : TObject);
      procedure MakePartialMove(S : TSegment; Dir : Integer; ChosenVertex : TVertex);
      procedure EndGame(Winner : Integer);
      procedure UndoMove;
      procedure RepeatMove;
      procedure UndoSegment;
      procedure RepeatSegment;
      procedure RefreshGameInfo;
      procedure GiveHint;
      function GetMoveToSave : PMoveList;
      procedure Start;
    end;

implementation

const
     NOT_YOUR_MOVE_MESSAGE = 'Nie wolno grać za komputer.';

  function createMoveData(Segment : TSegment; Direction : Integer; IsEndMove : Boolean) : PMoveList;
  var move : PMoveList;
  begin
      new(move);
      move^.Segment := Segment;
      move^.Direction:= Direction;
      move^.IsEndMove:= IsEndMove;
      createMoveData := move;
  end;

{ POMOCNICZE METODY DLA STOSU }

  constructor TStack.Create;
  begin
      Stack := nil;
  end;

  function TStack.Pop : PMoveList;
  begin
      Pop := Stack;
      Stack := Stack^.Next;
  end;

  function TStack.Top : PMoveList;
  begin
      Top := Stack;
  end;

  procedure TStack.Push(move : PMoveList);
  begin
       move^.Next:=Stack;
       Stack:=move;
  end;

  function TStack.IsEmpty : Boolean;
  begin
       IsEmpty := Stack = nil;
  end;

  procedure TStack.RemoveAll;
  var dummyList : PMoveList;
  begin
       while not IsEmpty do begin
         dummyList := Stack;
         Stack := Stack^.Next;
         if dummyList^.Segment <> nil then
             FreeAndNil(dummyList^.Segment);
         dispose(dummyList);
       end;
  end;

  destructor TStack.Destroy;
  begin
       RemoveAll;
  end;

{ KONIEC POMOCNICZYCH FUNKCJI STOSU }


{ FUNKCJE HISTORII GRY }
function TGameHistory.GetMoveToSave : PMoveList;
// tej funkcji powinno sie uzywac TYLKO do zapisywania gry
begin
     GetMoveToSave := DoneMoves.Top;
end;

procedure TGameHistory.ClearRepeatHistory;
begin
     UnDoneMoves.RemoveAll;
     hasNextMove:=false;
     hasNextSeg:=false;
end;

procedure TGameHistory.ClearUndoHistory;
begin
     DoneMoves.RemoveAll;
     hasPrevMove:=false;
     hasPrevSeg:=false;
end;

procedure TGameHistory.ClearHistory;
begin
     ClearRepeatHistory;
     ClearUndoHistory;
end;

destructor TGameHistory.Destroy;
begin
     ClearHistory;
     FreeAndNil(DoneMoves);
     FreeAndNil(UnDoneMoves);
end;

constructor TGameHistory.Create;
begin
     DoneMoves := TStack.Create;
     UnDoneMoves := TStack.Create;
end;

function TGameHistory.GetNextPartialMove : PMoveList;
begin
     GetNextPartialMove := UnDoneMoves.Pop;
     hasNextSeg:=(not UnDoneMoves.IsEmpty);
     hasNextMove:=(not UnDoneMoves.IsEmpty);
end;

function TGameHistory.GetPreviousPartialMove : PMoveList;
begin
     GetPreviousPartialMove := DoneMoves.Pop;
     hasPrevSeg:=(not DoneMoves.IsEmpty);
     hasPrevMove:=(not DoneMoves.IsEmpty);
end;

procedure TGameHistory.AddSegmentToUndo(moveData : PMoveList);
begin
     DoneMoves.Push(moveData);
     hasPrevSeg := true;
     hasPrevMove := true;
end;

procedure TGameHistory.AddSegmentToRepeat(moveData : PMoveList);
begin
     UnDoneMoves.Push(moveData);
     hasNextSeg := true;
     hasNextMove:=true;
end;

{ KONIEC FUNKCJI HISTORII GRY }

{ FUNKCJE GRY }
function TGame.GetMoveToSave : PMoveList;
begin
     GetMoveToSave := History.GetMoveToSave;
end;

procedure TGame.RefreshGameInfo;
const
     UNDO_SEGMENT_BUTTON_NAME = 'UndoSegmentButton';
     UNDO_MOVE_BUTTON_NAME = 'UndoMoveButton';
     REPEAT_SEGMENT_BUTTON_NAME = 'RepeatSegmentButton';
     REPEAT_MOVE_BUTTON_NAME = 'RepeatMoveButton';
     CURRENT_PLAYER_LABEL_NAME = 'CurrentPlayerLabel';
     HINT_BUTTON_NAME = 'HintButton';
     EDITOR_BUTTON_NAME = 'EditorButton';
     SAVE_BUTTON_NAME = 'SaveButton';


begin
     GameInfo.FindChildControl(UNDO_SEGMENT_BUTTON_NAME).Enabled := History.hasDoneSegments and (Player[CurrentPlayerId].IsHuman or Player[(CurrentPlayerId+1) mod 2].isHuman) and not GameEnded;
     GameInfo.FindChildControl(UNDO_MOVE_BUTTON_NAME).Enabled := History.hasDoneMoves and (Player[CurrentPlayerId].IsHuman or Player[(CurrentPlayerId+1) mod 2].isHuman) and not GameEnded;
     GameInfo.FindChildControl(REPEAT_SEGMENT_BUTTON_NAME).Enabled := History.hasUnDoneSegments and (Player[CurrentPlayerId].IsHuman or Player[(CurrentPlayerId+1) mod 2].isHuman) and not GameEnded;
     GameInfo.FindChildControl(REPEAT_MOVE_BUTTON_NAME).Enabled := History.hasUnDoneMoves and (Player[CurrentPlayerId].IsHuman or Player[(CurrentPlayerId+1) mod 2].isHuman) and not GameEnded;
     GameInfo.FindChildControl(CURRENT_PLAYER_LABEL_NAME).Caption := 'Player '+Player[CurrentPlayerId].playerName;
     if CurrentPlayerId = 0 then
         GameInfo.FindChildControl(CURRENT_PLAYER_LABEL_NAME).Font.Color:=clBlue
     else
         GameInfo.FindChildControl(CURRENT_PLAYER_LABEL_NAME).Font.Color:=clRed;
     GameInfo.FindChildControl(HINT_BUTTON_NAME).Enabled := Player[CurrentPlayerId].IsHuman and not GameEnded;
     GameInfo.FindChildControl(EDITOR_BUTTON_NAME).Enabled := Player[CurrentPlayerId].IsHuman and not GameEnded;
     GameInfo.FindChildControl(SAVE_BUTTON_NAME).Enabled := Player[CurrentPlayerId].IsHuman and not GameEnded;

end;

procedure TGame.UndoMove;
var list : PMoveList;
    oldBall : TVertex;
begin
    repeat
    list := History.GetPreviousPartialMove;
    if list^.IsEndMove then
       CurrentPlayerId:=(CurrentPlayerId+1) mod 2;
    GameField.DisconnectVertexInDirection(GameField.Ball, list^.Direction);
    oldBall := GameField.Ball;
    GameField.SetBall(GameField.GetVertexNeighbourInDirection(GameField.Ball, list^.Direction));
    oldBall.UnCheck;
    list^.Segment.Visible := false;
    History.AddSegmentToRepeat(list);
    until (History.DoneMoves.IsEmpty) or (History.DoneMoves.Top^.IsEndMove);
    RefreshGameInfo;
    if (not History.hasDoneMoves) and (not History.hasDoneSegments) then
        Player[CurrentPlayerId].MakeMove(GameField);
end;

procedure TGame.UndoSegment;
var list : PMoveList;
    oldBall : TVertex;
begin
    list := History.GetPreviousPartialMove;
    if list^.IsEndMove then
        CurrentPlayerId:=(CurrentPlayerId+1) mod 2;
    GameField.DisconnectVertexInDirection(GameField.Ball, list^.Direction);
    oldBall := GameField.Ball;
    GameField.SetBall(GameField.GetVertexNeighbourInDirection(GameField.Ball, list^.Direction));
    oldBall.UnCheck;
    list^.Segment.Visible := false;
    History.AddSegmentToRepeat(list);
    RefreshGameInfo;
end;

procedure TGame.RepeatMove;
var list : PMoveList;
begin
    repeat
        list := History.GetNextPartialMove;
        if list^.IsEndMove then
           CurrentPlayerId:=(CurrentPlayerId+1) mod 2;
        GameField.ConnectVertexInDirection(GameField.Ball, (list^.Direction + 4) mod 8);
        GameField.SetBall(GameField.GetVertexNeighbourInDirection(GameField.Ball, (list^.Direction + 4) mod 8));
        list^.Segment.Visible := true;
        History.AddSegmentToUndo(list);
    until (History.UnDoneMoves.IsEmpty) or (list^.IsEndMove);
    RefreshGameInfo;
end;

procedure TGame.RepeatSegment;
var list : PMoveList;
begin
    list := History.GetNextPartialMove;
    if list^.IsEndMove then
        CurrentPlayerId:=(CurrentPlayerId+1) mod 2;
    GameField.ConnectVertexInDirection(GameField.Ball, (list^.Direction + 4) mod 8);
    GameField.SetBall(GameField.GetVertexNeighbourInDirection(GameField.Ball, (list^.Direction + 4) mod 8));
    list^.Segment.Visible := true;
    History.AddSegmentToUndo(list);
    RefreshGameInfo;
end;

procedure TGame.MakePartialMove(S : TSegment; Dir : Integer; ChosenVertex : TVertex);
var moveData : PMoveList;
    moveEnded : Boolean;
begin
    GameEnded := (ChosenVertex.GetConnectionCount = 8)
               or (GameField.IsVertexInsideNet(ChosenVertex))
               or (not Player[CurrentPlayerId].canMakeMove)
               or (not Player[(CurrentPlayerId+1) mod 2].canMakeMove);
    moveEnded := (ChosenVertex.GetConnectionCount = 1) or GameEnded;
    moveData := createMoveData(S, Dir, moveEnded);
    History.AddSegmentToUndo(moveData);
    if moveEnded then
        CurrentPlayerId:=(CurrentPlayerId + 1) mod 2;
    History.ClearRepeatHistory;
    if GameField.IsVertexInsideNet(ChosenVertex) then begin
         if ChosenVertex.fposY < (GameField.FieldHeight div 2) then // pilka wbita od dolu
            EndGame(0)
         else                  // pilka wbita od gory
            EndGame(1);
    end else if ChosenVertex.GetConnectionCount = 8 then
       EndGame(CurrentPlayerId);
    RefreshGameInfo;
    Application.ProcessMessages;
    if not GameEnded and not HistoryLoading
       and Player[CurrentPlayerId].canMakeMove and Player[CurrentPlayerId].canMakeMove then begin
       Player[CurrentPlayerId].MakeMove(GameField);
    end;
end;

procedure TGame.EndGame(Winner : Integer);
const
     WIN_MESSAGE = 'The winner is player ';
begin
    case Winner of
         0 : ShowMessage(WIN_MESSAGE + Player[Winner].playerName);
         1 : ShowMessage(WIN_MESSAGE + Player[Winner].playerName);
    end;
    GameEnded:=true;
    RefreshGameInfo;
    Application.ProcessMessages;
end;

procedure TGame.OnChosenVertex(Sender : TObject);

var dirToBall, i : Integer;
    debugStr : String;
    s : TSegment;
    chosenVertex : TVertex;
begin
     if (not Player[CurrentPlayerId].IsHuman) and (History.hasNextSeg) then
         ShowMessage(NOT_YOUR_MOVE_MESSAGE)
     else if not(GameEnded) then begin
     chosenVertex := TVertex(Sender);
     if (chosenVertex <> GameField.Ball) and (chosenVertex.IsNeighbourOf(GameField.Ball)) then begin
        dirToBall := chosenVertex.DirectionTo(GameField.Ball);
        if chosenVertex.CanBeConnectedTo(GameField.Ball) then begin
             s := chosenVertex.MakeConnectionTo(GameField.Ball, Player[CurrentPlayerId].playerIndex);
             if not GameField.Ball.IsChecked then
                 GameField.Ball.Check;
             GameField.SetBall(chosenVertex);
             MakePartialMove(s, dirToBall, chosenVertex);
        end;
     end;
     end;
end;

procedure TGame.Start;
begin
     if GameField.Ball.GetConnectionCount < 8 then
        Player[CurrentPlayerId].MakeMove(GameField)
     else
        EndGame((CurrentPlayerId+1)mod 2);
end;

procedure TGame.GiveHint;
begin
     HintPlayer.MakeMove(GameField);
     RefreshGameInfo;
end;

constructor TGame.Create(AOwner : TWinControl; GameInfoPanel : TPanel; X, Y : Integer; IsPlayerOneHuman, IsPlayerTwoHuman : Boolean);
begin
     HistoryLoading:=false;
     GameEnded:=false;
     GameField := TField.Create(AOwner, X, Y, @OnChosenVertex);
     History := TGameHistory.Create;
     HintPlayer := TPlayerCPU.Create(0);
     CurrentPlayerId:=0;
     if IsPlayerOneHuman then
        Player[0] := TPlayerHuman.Create(0)
     else
        Player[0] := TPlayerCPU.Create(0);
     if IsPlayerTwoHuman then
        Player[1] := TPlayerHuman.Create(1)
     else
        Player[1] := TPlayerCPU.Create(1);
     GameInfo := GameInfoPanel;
     RefreshGameInfo;
end;

constructor TGame.CreateFromFieldTemplate(AOwner : TWinControl; GameInfoPanel : TPanel; FieldTemplate : TFieldTemplate; IsPlayerOneHuman, IsPlayerTwoHuman : Boolean);
begin
    HistoryLoading:=false;
    GameEnded:=false;
    GameField := TField.CreateFromTemplate(AOwner, FieldTemplate, @OnChosenVertex);
    History := TGameHistory.Create;
    HintPlayer := TPlayerCPU.Create(0);
    CurrentPlayerId:=0;
    if IsPlayerOneHuman then
       Player[0] := TPlayerHuman.Create(0)
    else
       Player[0] := TPlayerCPU.Create(0);
    if IsPlayerTwoHuman then
       Player[1] := TPlayerHuman.Create(1)
    else
       Player[1] := TPlayerCPU.Create(1);
    GameInfo := GameInfoPanel;
    RefreshGameInfo;
end;

destructor TGame.Destroy;
begin
    if not (Player[0].IsHuman) and not (Player[1].IsHuman) then begin
       Player[0].canMakeMove:=false;
       Player[1].canMakeMove:=false;
       EndGame(-1);
    end;
    Player[0].Destroy;
    Player[1].Destroy;
    GameField.Destroy;
    History.Destroy;
    HintPlayer.Destroy;
end;
{ KONIEC FUNKCJI GRY }
end.
