unit MainMenu;

{$mode objfpc}{$H+}

interface

uses
  Classes, SysUtils, FileUtil, Forms, Controls, Graphics, Dialogs, StdCtrls,
  ExtCtrls, Misc, DataParser, FieldClasses, GameSprites;

type

  { TFootball }

  TFootball = class(TForm)
    GameListLabel: TLabel;
    FieldPreviewLabel: TLabel;
    GameSavesList: TListBox;
    LoadGameContextMenu: TGroupBox;
    PlayButton: TButton;
    EditorInfoLabel: TLabel;
    FieldListLabel: TLabel;
    FieldTemplateList: TListBox;
    FieldPreviewPanel: TPanel;
    LoadSpecificGameButton: TButton;
    VersusLabel: TLabel;
    NewGameContextMenu: TGroupBox;
    LoadGameInfoLabel: TLabel;
    QuickPlayInfoLabel: TLabel;
    QuickPlayButton: TButton;
    NewGameButton: TButton;
    EditorButton: TButton;
    LoadGameButton: TButton;
    NewGameInfoLabel: TLabel;
    PlayerOneRadioGroup: TRadioGroup;
    PlayerTwoRadioGroup: TRadioGroup;
    procedure EditorButtonClick(Sender: TObject);
    procedure FieldTemplateListSelectionChange(Sender: TObject; User: boolean);
    procedure FormClose(Sender: TObject; var CloseAction: TCloseAction);
    procedure FormCreate(Sender: TObject);
    procedure GameSavesListSelectionChange(Sender: TObject; User: boolean);
    procedure LoadGameButtonClick(Sender: TObject);
    procedure LoadSpecificGameButtonClick(Sender: TObject);
    procedure NewGameButtonClick(Sender: TObject);
    procedure PlayButtonClick(Sender: TObject);
    procedure QuickPlayButtonClick(Sender: TObject);
  private
    FieldPreview : TField;
  public
    { public declarations }
  end;

var
  Football: TFootball;

implementation
uses
    GameForm, EditorForm, Game;
const
     FIELD_TEMPLATE_CORRUPTED = 'Field template is corrupted. Could not perform right action.';
{$R *.lfm}

{ TFootball }

procedure TFootball.QuickPlayButtonClick(Sender: TObject);
begin
  FootballGameForm.CurrentGame := TGame.Create(FootballGameForm.GamePanel, FootballGameForm.GameInfoPanel, 8, 10, true, false);
  Self.Hide;
  FootballGameForm.Show;
end;

procedure TFootball.NewGameButtonClick(Sender: TObject);
begin
  if LoadGameContextMenu.Visible then
     LoadGameContextMenu.Hide;
  NewGameContextMenu.Show;
end;

procedure TFootball.PlayButtonClick(Sender: TObject);
var
   newGame : TGame;
begin
   try
   newGame := TGame.CreateFromFieldTemplate(FootballGameForm.GamePanel, FootballGameForm.GameInfoPanel, DataManager.LoadFieldTemplate(FieldTemplateList.Items.Strings[FieldTemplateList.ItemIndex]),
                                            PlayerOneRadioGroup.ItemIndex = 0, PlayerTwoRadioGroup.ItemIndex = 0);
   FootballGameForm.CurrentGame := newGame;
   Self.Hide;
   FootballGameForm.Show;
   FootballGameForm.CurrentGame.Start;
   except
         ShowMessage(FIELD_TEMPLATE_CORRUPTED);
   end;
end;

procedure TFootball.EditorButtonClick(Sender: TObject);
begin
  Self.Hide;
  EditForm.Show;
end;

procedure TFootball.FieldTemplateListSelectionChange(Sender: TObject;
  User: boolean);
begin
  PlayButton.Enabled:=FieldTemplateList.ItemIndex >= 0;
  if FieldPreview <> nil then
     FieldPreview.Destroy;
  try
  FieldPreview := TField.CreateFromTemplate(FieldPreviewPanel, DataManager.LoadFieldTemplate(FieldTemplateList.Items.Strings[FieldTemplateList.ItemIndex]), nil);
  except
        ShowMessage(FIELD_TEMPLATE_CORRUPTED);
  end;
end;

procedure TFootball.FormClose(Sender: TObject; var CloseAction: TCloseAction);
begin
  DataManager.Destroy;
  if FieldPreview <> nil then
     FieldPreview.Destroy;
end;

procedure TFootball.FormCreate(Sender: TObject);
const
     GRAPHIC_LOAD_FAIL_MESSAGE = 'Graphics not found. Application is being terminated.';
var strList : TStringList;
    i : Integer;
begin
  try
  LoadGraphics(Football);
  except
        ShowMessage(GRAPHIC_LOAD_FAIL_MESSAGE);
        Application.Terminate;
  end;
  FieldPreview := nil;
  DataManager := TMyDataParser.Create;
  Cursor:=crHourGlass;
  Application.ProcessMessages;
  strList := FindAllFiles(GetCurrentDir+SLASH+TEMPLATE_FOLDER+SLASH, '*'+TEMPLATE_EXTENSION, true);
  for i := 0 to strList.Count-1 do
     FieldTemplateList.AddItem(ExtractFileNameOnly(strList.Strings[i]), nil);
  FreeAndNil(strList);
  strList := FindAllFiles(getCurrentDir+SLASH+SAVE_FOLDER+SLASH, '*'+SAVE_EXTENSION, true);
  for i := 0 to strList.Count-1 do
     GameSavesList.AddItem(ExtractFileNameOnly(strList.Strings[i]), nil);
  FreeAndNil(strList);
  Cursor := crDefault;
end;

procedure TFootball.GameSavesListSelectionChange(Sender: TObject; User: boolean);
begin
  LoadSpecificGameButton.Enabled:=GameSavesList.ItemIndex>=0;
end;

procedure TFootball.LoadGameButtonClick(Sender: TObject);
begin
   if NewGameContextMenu.Visible then
      NewGameContextMenu.Hide;
   LoadGameContextMenu.Show;
end;

procedure TFootball.LoadSpecificGameButtonClick(Sender: TObject);
const
     LOAD_GAME_DATA_CORRUPTED = 'Game save corrupted. Cannot load the game.';
var newGame : TGame;
begin
   try
   newGame := DataManager.LoadGame(FootballGameForm.GamePanel, FootballGameForm.GameInfoPanel, GameSavesList.Items.Strings[GameSavesList.ItemIndex]);
   FootballGameForm.CurrentGame := newGame;
   Self.Hide;
   FootballGameForm.Show;
   except
       ShowMessage(LOAD_GAME_DATA_CORRUPTED);
   end;
end;

end.

