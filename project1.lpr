program FootballGame;

{$mode objfpc}{$H+}

uses
  {$IFDEF UNIX}{$IFDEF UseCThreads}
  cthreads,
  {$ENDIF}{$ENDIF}
  Interfaces, // this includes the LCL widgetset
  Forms, GameForm, FieldClasses, Misc, GameSprites, game, Player, EditorForm,
  MainMenu, DataParser
  { you can add units after this };

{$R *.res}

begin
  randomize;
  RequireDerivedFormResource := True;
  Application.Initialize;
  Application.CreateForm(TFootball, Football);
  Application.CreateForm(TFootballGameForm, FootballGameForm);
  Application.CreateForm(TEditForm, EditForm);
  Application.Run;
end.

