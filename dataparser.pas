unit DataParser;

{$mode objfpc}{$H+}

interface

uses
  Classes, SysUtils, Controls, ExtCtrls, Dialogs, FieldClasses, Game, fpjson, fpjsonrpc, jsonparser, Misc;

type
    TMyDataParser = class
        function LoadGame(AOwner : TWinControl; GamePanel : TPanel; fileName : String) : TGame;
        procedure SaveGame(Game : TGame; fileName : String);
        function LoadFieldTemplate(fileName : String) : TFieldTemplate;
        function JSONObjectToFieldTemplate(jsonObject : TJSONObject) : TFieldTemplate;
        procedure SaveFieldTemplate(Field : TField; fileName : String);
        function GameMoveToJSONObject(Move : PMoveList) : TJSONObject;
        function CustomBorderToJSONObject(Border : PCustomConnectionList) : TJSONObject;
        function FieldToJSONObject(F : TField) : TJSONObject;
        function IsProperFileName(fileName : AnsiString) : Boolean;
    end;

const
    {file saving}
     MAX_FILE_LENGTH = 10;
     ALLOWED_CHARACTERS = ['a'..'z'] + ['A'..'Z'] + ['0'..'9'];
     TOO_LONG_MESSAGE = 'Preset name cannot be longer than ';
     TOO_LONG_MESSAGE_END = ' characters.';
     INVALID_CHARACTER_MESSAGE = 'Name can only contain letters and digits.';
     OVERWRITE_MESSAGE_DIALOG_NAME = 'Overwrite';
     OVERWRITE_MESSAGE_DIALOG_QUESTION = 'Preset with such name already found. Do you wish to overwrite already existing preset?';
     FILE_SAVED_SUCCESFULLY = 'File saved succesfully.';
     {/file saving}

var DataManager : TMyDataParser;

implementation

const
     {gra}
     JPLAYER_ONE_IS_HUMAN = 'p_one_is_human';
     JPLAYER_TWO_IS_HUMAN = 'p_two_is_human';
     JCURRENT_PLAYER_NUMBER = 'curr_p_no';
     JSTARTING_PLAYER_NUMBER = 'start_p_no';
     JFIELD = 'field';
     JMOVE_HISTORY = 'history';
     {/gra}

     {historia}
     JMOVE_DIRECTION = 'direction';
     JMOVE_IS_ENDING = 'ending';
     {/historia}

     {wzor boiska}
     JFIELD_BALL_POSX = 'ballx';
     JFIELD_BALL_POSY = 'bally';
     JFIELD_HEIGHT = 'fheight';
     JFIELD_WIDTH = 'fwidth';
     JFIELD_CUSTOM_BORDERS = 'cust_border_list';
     {/wzor boiska}

     {niestandardowy segment staly}
     JFROM_VERTEX_X = 'fromx';
     JFROM_VERTEX_Y = 'fromy';
     JTO_VERTEX_X = 'tox';
     JTO_VERTEX_Y = 'toy';
     {/nss}

function TMyDataParser.IsProperFileName(fileName : AnsiString) : Boolean;
var i : Integer;
begin
     if Length(fileName) > MAX_FILE_LENGTH then begin
        ShowMessage(TOO_LONG_MESSAGE + IntToStr(MAX_FILE_LENGTH) + TOO_LONG_MESSAGE_END);
        IsProperFileName := false;
     end else begin
         i := 1;
         IsProperFileName := true;
         while (i <= Length(fileName)) and IsProperFileName do begin
               if not (fileName[i] in ALLOWED_CHARACTERS) then begin
                  ShowMessage(INVALID_CHARACTER_MESSAGE);
                  IsProperFileName := false;
               end;
         inc(i);
         end;
     end;
end;

procedure WriteToFile(J : TJSONData; fileName : String);
var
   f : Text;
begin
    Assign(f, fileName);
    Rewrite(f);
    Write(f, UTF8Encode(J.FormatJSON()));
    Close(f);

end;

function TMyDataParser.JSONObjectToFieldTemplate(jsonObject : TJSONObject) : TFieldTemplate;
var
   dummyJsonObject : TJSONObject;
   dummyJsonArray : TJSONArray;
   fieldTemplate : TFieldTemplate;
   i : Integer;
begin
   fieldTemplate := TFieldTemplate.Create;
   fieldTemplate.BallPosX := jsonObject.Integers[JFIELD_BALL_POSX];
   fieldTemplate.BallPosY:= jsonObject.Integers[JFIELD_BALL_POSY];
   fieldTemplate.FieldHeight:=jsonObject.Integers[JFIELD_HEIGHT];
   fieldTemplate.FieldWidth:=jsonObject.Integers[JFIELD_WIDTH];
   dummyJsonArray := jsonObject.Arrays[JFIELD_CUSTOM_BORDERS];
   for i := 0 to dummyJsonArray.Count-1 do begin
       dummyJsonObject := TJSONObject(dummyJsonArray.Items[i]);
       fieldTemplate.AddCustomConnection(dummyJsonObject.Integers[JFROM_VERTEX_X], dummyJsonObject.Integers[JFROM_VERTEX_Y],
                                         dummyJsonObject.Integers[JTO_VERTEX_X], dummyJsonObject.Integers[JTO_VERTEX_Y]);
   end;
   JSONObjectToFieldTemplate := fieldTemplate;
end;

function TMyDataParser.LoadFieldTemplate(fileName : String) : TFieldTemplate;
var jsonStream : TJSONParser;
    jsonObject : TJSONObject;
    F : TFileStream;
begin
    F:=TFileStream.Create(GetCurrentDir+SLASH+TEMPLATE_FOLDER+SLASH+fileName+TEMPLATE_EXTENSION,fmopenRead);
    jsonStream := TJSONParser.Create(F);
    jsonObject := TJSONObject(jsonStream.Parse);
    LoadFieldTemplate := JSONObjectToFieldTemplate(jsonObject);
    FreeAndNil(jsonStream);
    FreeAndNil(F);
    FreeAndNil(jsonObject);
end;

procedure TMyDataParser.SaveFieldTemplate(Field : TField; fileName : String);
var jsonObject : TJSONObject;
begin
    jsonObject := FieldToJSONObject(Field);
    WriteToFile(jsonObject, getCurrentDir + SLASH + TEMPLATE_FOLDER + SLASH + fileName + TEMPLATE_EXTENSION);
    FreeAndNil(jsonObject);
end;

function TMyDataParser.LoadGame(AOwner: TWinControl; GamePanel: TPanel; fileName : String) : TGame;
var
    newGame : TGame;
    jsonParser : TJSONParser;
    jsonObject : TJSONObject;
    fileStream : TFileStream;
    fieldTemplate : TFieldTemplate;
    i : Integer;
    dummyVertex : TVertex;
begin
    fileStream:=TFileStream.Create(GetCurrentDir+SLASH+SAVE_FOLDER+SLASH+fileName+SAVE_EXTENSION,fmopenRead);
    jsonParser := TJSONParser.Create(fileStream);
    jsonObject := TJSONObject(jsonParser.Parse);
    jsonParser.Free;
    fieldTemplate := JSONObjectToFieldTemplate(jsonObject.Objects[JFIELD]);
    newGame := TGame.CreateFromFieldTemplate(AOwner, GamePanel, fieldTemplate, jsonObject.Booleans[JPLAYER_ONE_IS_HUMAN], jsonObject.Booleans[JPLAYER_TWO_IS_HUMAN]);
    newGame.HistoryLoading:=true;
    for i := jsonObject.Arrays[JMOVE_HISTORY].Count-1 downto 0 do begin
        dummyVertex := newGame.GameField.GetVertexNeighbourInDirection(newGame.GameField.Ball, (jsonObject.Arrays[JMOVE_HISTORY].Objects[i].Integers[JMOVE_DIRECTION] + 4) mod 8);
        dummyVertex.OnClick(dummyVertex);
    end;
    newGame.HistoryLoading:=false;
    LoadGame := newGame;
    jsonObject.Free;
    FreeAndNil(fileStream);
end;



procedure TMyDataParser.SaveGame(Game : TGame; fileName : String);

var jsonObject : TJSONObject;
    jsonMoveListArray : TJSONArray;
    startingPlayer : integer;
    move : PMoveList;
begin
     jsonObject := TJSONObject.Create;
     jsonObject.Add(JPLAYER_ONE_IS_HUMAN, Game.Player[0].IsHuman);
     jsonObject.Add(JPLAYER_TWO_IS_HUMAN, Game.Player[1].IsHuman);
     jsonObject.Add(JFIELD, FieldToJSONObject(Game.GameField));
     jsonObject.Add(JCURRENT_PLAYER_NUMBER, Game.CurrentPlayerId);
     jsonMoveListArray := TJSONArray.Create;
     move := Game.GetMoveToSave;
     startingPlayer:=Game.CurrentPlayerId;
     while (move <> nil) do begin
           if move^.IsEndMove then
              startingPlayer := (startingPlayer + 1) mod 2;
           jsonMoveListArray.Add(GameMoveToJSONObject(move));
           move := move^.Next;
     end;
     jsonObject.Add(JSTARTING_PLAYER_NUMBER, startingPlayer);
     jsonObject.Add(JMOVE_HISTORY, jsonMoveListArray);
     WriteToFile(jsonObject, GetCurrentDir+SLASH+SAVE_FOLDER+SLASH+fileName+SAVE_EXTENSION);
     jsonObject.Free;
end;

function TMyDataParser.GameMoveToJSONObject(Move : PMoveList) : TJSONObject;

var jsonResult : TJSONObject;
begin
     jsonResult := TJSONObject.Create;
     jsonResult.Add(JMOVE_DIRECTION, Move^.Direction);
     jsonResult.Add(JMOVE_IS_ENDING, Move^.IsEndMove);
     GameMoveToJSONObject := jsonResult;
end;

function TMyDataParser.CustomBorderToJSONObject(Border : PCustomConnectionList) : TJSONObject;

var jsonResult : TJSONObject;
begin
     jsonResult := TJSONObject.Create;
     jsonResult.Add(JFROM_VERTEX_X, Border^.fromVertexX);
     jsonResult.Add(JTO_VERTEX_X, Border^.toVertexX);
     jsonResult.Add(JFROM_VERTEX_Y, Border^.fromVertexY);
     jsonResult.Add(JTO_VERTEX_Y, Border^.toVertexY);
     CustomBorderToJSONObject := jsonResult;
end;

function TMyDataParser.FieldToJSONObject(F : TField) : TJSONObject;
var jsonField : TJSONObject;
    jsonConnectionArray : TJSONArray;
    customConnectionList : PCustomConnectionList;
begin
     jsonField := TJSONObject.Create;
     jsonConnectionArray := TJSONArray.Create;
     jsonField.Add(JFIELD_BALL_POSX, F.Template.BallPosX);
     jsonField.Add(JFIELD_BALL_POSY, F.Template.BallPosY);
     jsonField.Add(JFIELD_HEIGHT, F.Template.FieldHeight);
     jsonField.Add(JFIELD_WIDTH, F.Template.FieldWidth);
     customConnectionList := F.Template.GetCustomBorderToSave;
     while customConnectionList <> nil do begin
           jsonConnectionArray.Add(CustomBorderToJSONObject(customConnectionList));
           customConnectionList:=customConnectionList^.Next;
     end;
     jsonField.Add(JFIELD_CUSTOM_BORDERS, jsonConnectionArray);
     FieldToJSONObject := jsonField;
end;

end.

