unit GameSprites;

{$mode objfpc}{$H+}

interface

uses
  Classes, SysUtils, Controls, Graphics, Dialogs, StdCtrls,
  ExtCtrls, Misc;

procedure LoadGraphics(AOwner : TComponent);

const
     DEFAULT_GRAPHIC_SIZE = 32;
     DEFAULT_GRAPHIC_EXTENSION = '.png';
var
   BasicSegmentGraphics : array[TDirectionEnum] of TImage;
   P1SegmentGraphics : array[TDirectionEnum] of TImage;
   P2SegmentGraphics : array[TDirectionEnum] of TImage;
   VertexGraphics : array[0..3] of TImage;
   BallGraphic : TImage;



implementation

procedure LoadGraphics(AOwner : TComponent);

const
     HORIZONTAL_SEGMENT_FILENAME = 'horizontal';
     VERTICAL_SEGMENT_FILENAME = 'vertical';
     LRTB_SEGMENT_FILENAME = 'LRTB';
     LRBT_SEGMENT_FILENAME = 'LRBT';
     VERTEX_BASE_FILENAME = 'vertBase';
     VERTEX_BASE_CHOSEN_FILENAME = 'vertBaseChosen';
     VERTEX_INNER_FILENAME = 'vertInner';
     VERTEX_INNER_CHOSEN_FILENAME = 'vertInnerChosen';

          function getFileName(dir : TDirectionEnum) : String;

          begin
          case dir of
               HORIZONTAL : getFileName := HORIZONTAL_SEGMENT_FILENAME;
               VERTICAL : getFileName := VERTICAL_SEGMENT_FILENAME;
               LRTB : getFileName := LRTB_SEGMENT_FILENAME;
               LRBT : getFileName := LRBT_SEGMENT_FILENAME;
          end;
         end;

var d : TDirectionEnum;
    i : Integer;
    path : String;
begin
    for d := HORIZONTAL to LRBT do begin
        BasicSegmentGraphics[d] := TImage.Create(AOwner);
        BasicSegmentGraphics[d].Picture.LoadFromFile(getCurrentDir+SLASH+SPRITE_FOLDER+SLASH+SEGMENT_FOLDER + SLASH +getFileName(d)+'Base.png');
        P1SegmentGraphics[d] := TImage.Create(AOwner);
        P1SegmentGraphics[d].Picture.LoadFromFile(getCurrentDir+SLASH+SPRITE_FOLDER+SLASH+SEGMENT_FOLDER + SLASH +getFileName(d)+'P1.png');
        P2SegmentGraphics[d] := TImage.Create(AOwner);
        P2SegmentGraphics[d].Picture.LoadFromFile(getCurrentDir+SLASH+SPRITE_FOLDER+SLASH+SEGMENT_FOLDER + SLASH +getFileName(d)+'P2.png');
    end;
    for i := 0 to 3 do begin
        VertexGraphics[i] := TImage.Create(AOwner);
        path := getCurrentDir+SLASH+SPRITE_FOLDER+SLASH+VERTEX_FOLDER + SLASH;
        case i of
             0 : path := path + VERTEX_BASE_FILENAME + DEFAULT_GRAPHIC_EXTENSION;
             1 : path := path + VERTEX_BASE_CHOSEN_FILENAME + DEFAULT_GRAPHIC_EXTENSION;
             2 : path := path + VERTEX_INNER_FILENAME + DEFAULT_GRAPHIC_EXTENSION;
             3 : path := path + VERTEX_INNER_CHOSEN_FILENAME + DEFAULT_GRAPHIC_EXTENSION;
        end;
        VertexGraphics[i].Picture.LoadFromFile(path);
    end;
    BallGraphic := Timage.Create(AOwner);
    BallGraphic.Picture.LoadFromFile(GetCurrentDir + SLASH + SPRITE_FOLDER + SLASH + VERTEX_FOLDER + SLASH + 'vertBall.png');
end;

end.

