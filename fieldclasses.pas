unit FieldClasses;

{$mode objfpc}{$H+}

{
 TODO : Ladowanie grafik procedurami a nie przypisaniem
        Pozbycie sie zaleznosci od Player (a moze i od Misc)
        Przemyslec sobie czy na pewno TDirectionEnum jest potrzebne

}
interface

uses
  Classes, SysUtils, GameSprites, Controls, Graphics, Dialogs, {StdCtrls,}
  ExtCtrls, Misc;

type
    PCustomConnectionList = ^TCustomConnectionList;
    TCustomConnectionList = record
        fromVertexX, fromVertexY, toVertexX, toVertexY : Integer;
        Next : PCustomConnectionList;
    end;
    TSegment = class(TImage)
        constructor Create(AOwner : TComponent; Direction : TDirectionEnum; PlayerOwner : TPlayerEnum; GraphicSize : Integer);
        destructor Destroy; override;
        private
               mdirection : TDirectionEnum;
        public
               property direction : TDirectionEnum Read mdirection;
    end;
    TVertex = class(TImage)
        type TVertexPlacement = (BORDER, INNER);
        constructor Create(AOwner : TComponent; Placement : TVertexPlacement; X, Y : Integer; OnClickEvent : TNotifyEvent; GraphicSize : Integer);
        destructor Destroy; override;
        function IsChecked : Boolean;
        procedure Check;
        procedure UnCheck;
        procedure RefreshImage;
        procedure SetPlacement(Placement : TVertexPlacement);
        function IsNeighbourOf(V : TVertex) : Boolean;
        function IsConnectedInDirection(Direction : Integer) : Boolean;
        function DirectionTo(V : TVertex) : Integer;
        function CanBeConnectedTo(V : TVertex) : Boolean;
        function MakeConnectionTo(V : TVertex; Player : TPlayerEnum) : TSegment;
        function GetConnectionCount : Integer;
        private
               checked : Boolean;
               pictureId : 0..3;
               connections : Array[0..7] of Boolean;
               posX, posY : Integer;
        public
              property fposX : Integer Read posX;
              property fposY : Integer Read posY;
    end;
    TFieldTemplate = class
        constructor Create;
        destructor Destroy;
        function GetCustomBorderToSave : PCustomConnectionList;
        procedure AddCustomConnection(fromX, fromY, toX, toY : Integer);
        private
               mFieldWidth, mFieldHeight, mBallPosX, mBallPosY : Integer;
               customConnectionList : PCustomConnectionList;
        public
              property FieldWidth : Integer Read mFieldWidth Write mFieldWidth;
              property FieldHeight : Integer Read mFieldHeight Write mFieldHeight;
              property BallPosX : Integer Read mBallPosX Write mBallPosX;
              property BallPosY : Integer Read mBallPosY Write mBallPosY;
    end;
    TField = class
        type PSegmentList = ^TSegmentList;
             TSegmentList = record
                  Segment : TSegment;
                  Next : PSegmentList;
             end;
        constructor Create(AOwner : TWinControl; const FieldWidth, FieldHeight : Integer; OnVertexClickEvent : TNotifyEvent);
        constructor CreateFromTemplate(AOwner : TWinControl; FromTemplate : TFieldTemplate; OnVertexClickEvent : TNotifyEvent);
        destructor Destroy;
        procedure SetBall(V : TVertex);
        function GetVertexNeighbourInDirection(V : TVertex; Dir : Integer) : TVertex;
        procedure DisconnectVertexInDirection(V : TVertex; Dir : Integer);
        procedure ConnectVertexInDirection(V : TVertex; Dir : Integer);
        procedure AddSegmentToBorderList(S : TSegment);
        function IsVertexInsideNet(V : TVertex) : Boolean;
        function GetVertexAtPos(X, Y : Integer) : TVertex;
        private
               mGraphicSize : Integer;
               mBorders : PSegmentList;
               mVertice : array of array of TVertex;
               mBall : TVertex;
               mFieldWidth, mFieldHeight : Integer;
               mTemplate : TFieldTemplate;
               procedure PlaceEverything(AOwner : TWinControl; const FieldWidth, FieldHeight : Integer; OnVertexClickEvent : TNotifyEvent; generateTemplate : Boolean);
        public
              property GraphicSize : Integer Read mGraphicSize;
              property Ball : TVertex Read mBall;
              property FieldWidth : Integer Read mFieldWidth;
              property FieldHeight : Integer Read mFieldHeight;
              property Template : TFieldTemplate Read mTemplate Write mTemplate;
    end;

implementation

{ FUNKCJE DLA WEZLA }
function TVertex.IsConnectedInDirection(Direction : Integer) : Boolean;
begin
    IsConnectedInDirection := connections[Direction] = true;
end;

procedure TVertex.SetPlacement(Placement : TVertexPlacement);
begin
     case Placement of
          INNER : if checked then Self.pictureId := 3 else Self.pictureId := 2;
          BORDER : if checked then Self.pictureId := 1 else Self.pictureId := 0;
     end;
     RefreshImage;
end;

destructor TVertex.Destroy;
begin
     Inherited Destroy;
end;

function TVertex.IsChecked : Boolean;

begin
     IsChecked := Self.checked;
end;

procedure TVertex.Check;

begin
     Self.checked := true;
     inc(Self.pictureId);
     Self.Picture := VertexGraphics[Self.pictureId].Picture;
end;

procedure TVertex.UnCheck;

begin
     Self.checked := false;
     dec(Self.pictureId);
     Self.Picture := VertexGraphics[Self.pictureId].Picture;
end;

procedure TVertex.RefreshImage;

begin
     Self.Picture := VertexGraphics[Self.pictureId].Picture;
end;

function TVertex.IsNeighbourOf(V : TVertex) : Boolean;
var DistanceX, DistanceY : Integer;
begin
     DistanceX := V.posX - posX;
     if DistanceX < 0 then
        DistanceX := - DistanceX;
     DistanceY := V.posY - posY;
     if DistanceY < 0 then
        DistanceY := -DistanceY;
     IsNeighbourOf := (DistanceX <= 1) and (DistanceY <= 1);
end;

function TVertex.DirectionTo(V : TVertex) : Integer;
begin
     if (V.posX - posX = 1) and (V.posY - posY = 0) then
        DirectionTo := 0
     else
        DirectionTo := 4 + (2 + V.posX - posX) * (V.posY - posY);
end;

function TVertex.CanBeConnectedTo(V : TVertex) : Boolean;
var dirTo : Integer;
begin
     dirTo := Self.DirectionTo(V);
     CanBeConnectedTo := (not Self.connections[dirTo] and not V.connections[(dirTo + 4) mod 8]);
end;

function TVertex.GetConnectionCount : Integer;
var dummy, i : Integer;
begin
     dummy := 0;
     for i := 0 to 7 do
         if connections[i] then
             inc(dummy);
     GetConnectionCount := dummy;
end;

function TVertex.MakeConnectionTo(V : TVertex; Player : TPlayerEnum) : TSegment;

    function DrawConnection(RelativeDir : Integer; Ball : TVertex; Player : TPlayerEnum) : TSegment;
    var s : TSegment;
        v : TVertex;
        begin
          if (RelativeDir < 4) then
              v := Ball
          else
              v := Self;
          case RelativeDir of
              1,5 :
              begin
                  s := TSegment.Create(Self.Parent, LRBT, Player, v.Height);
                  s.Left := v.Left - v.Width div 2;
                  s.Top := v.Top + v.Height div 2;
              end;
              3,7 :
              begin
                  s := TSegment.Create(Self.Parent, LRTB, Player, v.Height);
                  s.Left := v.Left + v.Width div 2;
                  s.Top := v.Top + v.Height div 2;
              end;
              2,6 :
              begin
                  s := TSegment.Create(Self.Parent, VERTICAL, Player, v.Height);
                  s.Left := v.Left;
                  s.Top := v.Top + v.Height div 2;
              end;
              0,4 :
              begin
                  s := TSegment.Create(Self.Parent, HORIZONTAL, Player, v.Height);
                  s.Left := v.Left - v.Width div 2;
                  s.Top := v.Top;
              end;
          end;
          s.Parent := Self.Parent;
          s.SendToBack;
          DrawConnection := s;
        end;

var relativeDir : Integer;

begin
   relativeDir := DirectionTo(V);
   connections[relativeDir] := true;
   V.connections[(relativeDir + 4) mod 8] := true;
   MakeConnectionTo := DrawConnection(relativeDir, V, Player);
end;

constructor TVertex.Create(AOwner : TComponent; Placement : TVertexPlacement; X, Y : Integer; OnClickEvent : TNotifyEvent; GraphicSize : Integer);
var i : Integer;
begin
     Inherited Create(AOwner);
     Self.Width:=GraphicSize;
     Self.Height:=GraphicSize;
     Self.Proportional:=true;
     Self.posX := X;
     Self.posY := Y;
     if Placement = INNER then
        Self.pictureId := 2
     else
        Self.pictureId := 0;
     Self.Picture := VertexGraphics[Self.pictureId].Picture;
     Self.checked := false;
     Self.OnClick := OnClickEvent;
     for i := 0 to 7 do
         Self.connections[i] := false;
end;

{ KONIEC FUNKCJI DLA WEZLA }
{ FUNKCJE DLA SEGMENTU }

destructor TSegment.Destroy;
begin
     Inherited Destroy;
end;

constructor TSegment.Create(AOwner : TComponent; Direction : TDirectionEnum; PlayerOwner : TPlayerEnum; GraphicSize : Integer);

begin
     Inherited Create(AOwner);
     Self.mdirection := Direction;
     Width:=GraphicSize;
     Height:=GraphicSize;
     Proportional:=true;
     case PlayerOwner of
          P1 : Picture := P1SegmentGraphics[Direction].Picture;
          P2 : Picture := P2SegmentGraphics[Direction].Picture;
          NONE : Picture := BasicSegmentGraphics[Direction].Picture;
     end;
end;

{ KONIEC FUNKCJI DLA SEGMENTU }
{ FUNKCJE DLA WZORU BOISKA }

constructor TFieldTemplate.Create;
begin
  customConnectionList:=nil;
  BallPosY:=6;
  BallPosX:=5;
  FieldHeight:=10;
  FieldWidth:=8;
end;

procedure TFieldTemplate.AddCustomConnection(fromX, fromY, toX, toY : Integer);
var
    dummyList : PCustomConnectionList;
begin
    new(dummyList);
    dummyList^.fromVertexX:=fromX;
    dummyList^.fromVertexY:=fromY;
    dummyList^.toVertexX:=toX;
    dummyList^.toVertexY:=toY;
    dummyList^.Next:=customConnectionList;
    customConnectionList:=dummyList;
end;

function TFieldTemplate.GetCustomBorderToSave : PCustomConnectionList;
begin
     GetCustomBorderToSave := customConnectionList;
end;

destructor TFieldTemplate.Destroy;
var dummyList : PCustomConnectionList;
begin
    while customConnectionList <> nil do begin
         dummyList := customConnectionList;
         customConnectionList:=customConnectionList^.Next;
         dispose(dummyList);
    end;
end;

{ KONIEC FUNKCJI DLA WZORU BOISKA }
{ FUNKCJE DLA BOISKA }

function TField.GetVertexAtPos(X, Y : Integer) : TVertex;
begin
     if (X < Length(mVertice)) and (Y < Length(mVertice[0])) then
        GetVertexAtPos := mVertice[X][Y]
     else
        GetVertexAtPos := nil;
end;

destructor TField.Destroy;
var i, j : Integer;
    dummySegmentList : PSegmentList;
begin
     for i := 1 to FieldWidth+1 do
         for j := 1 to FieldHeight+1 do
             mVertice[i][j].Destroy;
     for i := 1 to 2 do
         for j := 1 to 3 do
              mVertice[FieldWidth div 2 - 1 + j][(FieldHeight+2)*(i div 2)].Destroy;
     while (mBorders^.Next <> nil) do begin
         dummySegmentList:=mBorders;
         mBorders:=mBorders^.Next;
         dummySegmentList^.Segment.Destroy;
         dispose(dummySegmentList);
     end;
     dispose(mBorders);
     if mTemplate <> nil then
        mTemplate.Destroy;
     inherited;
end;

function TField.IsVertexInsideNet(V : TVertex) : Boolean;
begin
    IsVertexInsideNet := (V.posY = 0) or (V.posY = FieldHeight+2);
end;

function TField.GetVertexNeighbourInDirection(V : TVertex; Dir : Integer) : TVertex;
var diffX, diffY : Integer;
begin
     diffX := 0;
     diffY := 0;
     case Dir of
          0, 1, 7 : inc(diffX);
          3, 4, 5 : dec(diffX);
     end;
     case Dir of
          1, 2, 3 : dec(diffY);
          5, 6, 7 : inc(diffY);
     end;
     GetVertexNeighbourInDirection := mVertice[V.posX + diffX, V.posY + diffY];
end;

procedure TField.ConnectVertexInDirection(V : TVertex; Dir : Integer);
begin
     V.connections[Dir] := true;
     GetVertexNeighbourInDirection(V, Dir).connections[(Dir + 4) mod 8] := true;
end;

procedure TField.DisconnectVertexInDirection(V : TVertex; Dir : Integer);
begin
     V.connections[Dir] := false;
     GetVertexNeighbourInDirection(V, Dir).connections[(Dir + 4) mod 8] := false;
end;

procedure TField.SetBall(V : TVertex);
begin
    mBall.RefreshImage;
    if Ball.IsNeighbourOf(V) and not Ball.IsChecked then
        Ball.Check
    else if (not Ball.IsNeighbourOf(V)) and (Ball.IsChecked) then
        Ball.UnCheck;
    mBall := V;
    mBall.Picture := BallGraphic.Picture;
end;

procedure TField.AddSegmentToBorderList(S : TSegment);
var dummySegmentList : PSegmentList;
begin
     new(dummySegmentList);
     dummySegmentList^.Segment := s;
     dummySegmentList^.Next := mBorders;
     mBorders := dummySegmentList;
     dummySegmentList:=nil;
end;


procedure TField.PlaceEverything(AOwner : TWinControl; const FieldWidth, FieldHeight : Integer; OnVertexClickEvent : TNotifyEvent; generateTemplate : Boolean);

var i, j, k, leftBound, rightBound, marginLeft, marginTop, actualGraphicSize : Integer;
   s : TSegment;
   v : TVertex;
begin
  actualGraphicSize := DEFAULT_GRAPHIC_SIZE;
  marginLeft := (AOwner.Width - (FieldWidth+1) * DEFAULT_GRAPHIC_SIZE) div 2 - DEFAULT_GRAPHIC_SIZE div 4;
  if marginLeft < 0 then
     marginLeft := DEFAULT_GRAPHIC_SIZE;
  marginTop := (AOwner.Height - (FieldHeight+3) * DEFAULT_GRAPHIC_SIZE) div 2 - DEFAULT_GRAPHIC_SIZE div 4;
  if marginTop < 0 then
     marginTop := DEFAULT_GRAPHIC_SIZE;
  if (marginLeft + (FieldWidth+1)*DEFAULT_GRAPHIC_SIZE) >= AOwner.Width then begin
     actualGraphicSize := AOwner.Width div (FieldWidth+3);
     marginLeft := actualGraphicSize;
     marginTop := (AOwner.Height - (FieldHeight+3) * actualGraphicSize) div 2 - actualGraphicSize div 4;
  end;
  if (marginTop + (FieldHeight+3) * actualGraphicSize) >= AOwner.Height then begin
     actualGraphicSize := AOwner.Height div (FieldHeight+5);
     marginTop := actualGraphicSize;
     marginLeft := (AOwner.Width - (FieldWidth+1) * actualGraphicSize) div 2 - actualGraphicSize div 4;
  end;
  mGraphicSize:=actualGraphicSize;
  SetLength(Self.mVertice, FieldWidth+3, FieldHeight+3);
  mFieldWidth:=FieldWidth;
  mFieldHeight:=FieldHeight;
  new(mBorders);
  mBorders^.Segment := nil;
  mBorders^.Next:= nil;
  for i:=1 to 2 do begin
      for j := 1 to 2 do begin // ustawiam bramki
          s := TSegment.Create(AOwner, HORIZONTAL, NONE, actualGraphicSize);
          s.Parent := AOwner;
          s.Left := marginLeft + (FieldWidth div 2 - 1) * actualGraphicSize + actualGraphicSize div 2 + actualGraphicSize * (j-1);
          s.Top := marginTop + (2 * actualGraphicSize + FieldHeight * actualGraphicSize) * (i-1);
          AddSegmentToBorderList(s);
          s := TSegment.Create(AOwner, VERTICAL, NONE, actualGraphicSize);
          s.Parent := AOwner;
          s.Left := marginLeft + (FieldWidth div 2 - 1) * actualGraphicSize + actualGraphicSize * 2 * (j div 2);
          s.Top := marginTop + actualGraphicSize div 2 + (actualGraphicSize + FieldHeight * actualGraphicSize) * (i-1);
          AddSegmentToBorderList(s);
      end;
      for j := 1 to FieldWidth do
          if (j <> FieldWidth div 2) and (j <> (FieldWidth div 2 + 1)) then begin // ustawiam segmenty poziome
             s := TSegment.Create(AOwner, HORIZONTAL, NONE, actualGraphicSize);
             s.Parent := AOwner;
             s.Left := marginLeft + actualGraphicSize div 2 + actualGraphicSize * (j-1);
             s.Top := actualGraphicSize + marginTop + FieldHeight * actualGraphicSize * (i-1);
             AddSegmentToBorderList(s);
          end;
      for j := 1 to FieldHeight do begin // ustawiam segmenty pionowe
          s := TSegment.Create(AOwner, VERTICAL, NONE, actualGraphicSize);
          s.Parent := AOwner;
          s.Left := marginLeft + FieldWidth * actualGraphicSize *(i-1);
          s.Top := marginTop + actualGraphicSize + actualGraphicSize div 2 + actualGraphicSize * (j-1);
          AddSegmentToBorderList(s);
      end;
  end;
  for i:=1 to FieldWidth+1 do // ustawiam wezly
      for j:= 1 to FieldHeight+1 do begin
          if ((j = 1) or (i = 1) or (j = FieldHeight+1) or (i = FieldWidth+1))
             and (i <> (FieldWidth div 2 + 1)) then begin
             v := TVertex.Create(AOwner, BORDER, i, j, OnVertexClickEvent, actualGraphicSize);
             if (i = 1) then begin
                if j > 1 then
                   leftBound := 2
                else
                   leftBound := 0;
                rightBound := 6 + 2*(j div (FieldHeight+1));
             end else if (i = (FieldWidth+1)) then begin
                 if (j > 1) then
                    rightBound := 10
                 else
                    rightBound := 12;
                 leftBound := 6 - 2*(j div (FieldHeight+1));
             end else if (i = FieldWidth div 2) and ((j = 1) or (j = FieldHeight+1)) then begin
                 leftBound := 2 + 2*(j div (FieldHeight+1));
                 rightBound := 4 + 2*(j div (FieldHeight+1));
             end else if (i = FieldWidth div 2 + 2) and ((j = 1) or (j = FieldHeight+1)) then begin
                 leftBound := 0 + 6*(j div (FieldHeight+1));
                 rightBound := 2 + 6*(j div (FieldHeight+1));
             end else begin
                 leftBound := 0 + 4*(j div (FieldHeight+1));
                 rightBound := 4 + 4*(j div (FieldHeight+1));
             end;
             for k := leftBound to rightBound do
                 v.connections[k mod 8] := true;
          end else
              v := TVertex.Create(AOwner, INNER, i, j, OnVertexClickEvent, actualGraphicSize);
          v.Parent := AOwner;
          v.Left := marginLeft + actualGraphicSize * (i-1);
          v.Top := marginTop + actualGraphicSize * j;
          mVertice[i,j] := v;
      end;
  for i:=1 to 2 do  // ustawiam wezly na bramkach
      for j:=1 to 3 do begin
          v := TVertex.Create(AOwner, BORDER, FieldWidth div 2 - 1 + j, (i div 2) * (FieldHeight+2), OnVertexClickEvent, actualGraphicSize);
          v.connections[0] := true;
          v.connections[4] := true;
          if (i = 1) then begin
             for k := 1 to 3 do
                 v.connections[k] := true;
             if (j <> 2) then begin
                v.connections[6] := true;
                if (j = 1) then
                    v.connections[5] := true
                else
                    v.connections[7] := true;
             end;
          end else begin
              for k := 5 to 7 do
                  v.connections[k] := true;
              if (j <> 2) then begin
                  v.connections[2] := true;
                  if (j = 1 ) then
                      v.connections[3] := true
                  else
                      v.connections[1] := true;
              end;
          end;
          v.Parent := AOwner;
          v.Left := marginLeft + (FieldWidth div 2 - 1) * actualGraphicSize + actualGraphicSize * (j-1);
          v.Top := marginTop + (2 * actualGraphicSize + FieldHeight * actualGraphicSize) * (i-1);
          mVertice[FieldWidth div 2 - 1 + j, (FieldHeight+2)*(i div 2)] := v;
      end;
    mBall := mVertice[FieldWidth div 2 + 1, FieldHeight div 2 + 1]; // ustawiam pilke
    mBall.Picture := BallGraphic.Picture;
    if generateTemplate then
       mTemplate := TFieldTemplate.Create;
end;

constructor TField.Create(AOwner : TWinControl; const FieldWidth, FieldHeight : Integer; OnVertexClickEvent : TNotifyEvent);

begin
     PlaceEverything(AOwner, FieldWidth, FieldHeight, OnVertexClickEvent, true);
end;

constructor TField.CreateFromTemplate(AOwner : TWinControl; FromTemplate : TFieldTemplate; OnVertexClickEvent : TNotifyEvent);
var s : TSegment;
    connectionList : PCustomConnectionList;
begin
     PlaceEverything(AOwner, FromTemplate.FieldWidth, FromTemplate.FieldHeight, OnVertexClickEvent, false);
     Ball.RefreshImage;
     if Ball.checked then
         Ball.UnCheck;
     connectionList := FromTemplate.customConnectionList;
     mTemplate := FromTemplate;
     while connectionList <> nil do begin
         s := GetVertexAtPos(connectionList^.fromVertexX, connectionList^.fromVertexY).MakeConnectionTo(GetVertexAtPos(connectionList^.toVertexX, connectionList^.toVertexY), TPlayerEnum.NONE);
         GetVertexAtPos(connectionList^.fromVertexX, connectionList^.fromVertexY).SetPlacement(TVertex.TVertexPlacement.BORDER);
         GetVertexAtPos(connectionList^.toVertexX, connectionList^.toVertexY).SetPlacement(TVertex.TVertexPlacement.BORDER);
         GetVertexAtPos(connectionList^.fromVertexX, connectionList^.fromVertexY).RefreshImage;
         GetVertexAtPos(connectionList^.toVertexX, connectionList^.toVertexY).RefreshImage;
         AddSegmentToBorderList(s);
         connectionList:=connectionList^.Next;
     end;
     SetBall(GetVertexAtPos(FromTemplate.BallPosX, FromTemplate.BallPosY));
end;

{ KONIEC FUNKCJI DLA BOISKA }

end.

